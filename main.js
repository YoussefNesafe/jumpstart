var ishow = document.getElementById("#show");

ishow.onclick = () => {
  ishow.nextElementSibling.classList.toggle("showhide");
};

//To Top Button
const backToTop = document.getElementById("toTop");

window.addEventListener("scroll", scrollFunction);

function scrollFunction() {
  if (window.pageYOffset > 300) {
    //Show Back To Top button
    if (!backToTop.classList.contains("zoomIn")) {
      backToTop.classList.remove("zoomOut");
      backToTop.classList.add("zoomIn");
      backToTop.style.display = "block";
    }
  } else {
    // Hide Back To Top button
    if (!backToTop.classList.contains("zoomOut")) {
      backToTop.classList.remove("zoomIn");
      backToTop.classList.add("zoomOut");
      setTimeout(function () {
        backToTop.style.display = "none";
      }, 1000);
    }
  }
}
backToTop.onclick = () => {
  window.scrollTo({
    left: 0,
    top: 0,
    behavior: "smooth",
  });
};
//Select All links in The navigation bar
const allLinks = document.querySelectorAll(".links a");

//Function for smooth scroll
function scroolToSomeWhere(elements) {
  elements.forEach((ele) => {
    ele.addEventListener("click", (e) => {
      e.preventDefault();
      document.querySelector(e.target.dataset.section).scrollIntoView({
        behavior: "smooth",
      });
    });
  });
}
scroolToSomeWhere(allLinks);

//Slider Images

//Get Image from Slider
var sliderImages = Array.from(document.querySelectorAll('.slider img'));

//Get Number of Slides
var slidesCount = sliderImages.length;

//Set Current Slide
var currentSlide = 2;

//Previous and Next Button
var next = document.querySelector('.arr-right');
var prev = document.querySelector('.arr-left');

//Handel Click on Previous and Next Button
next.onclick = nextSlide;
prev.onclick = prevSlide;

//Next Slide Function
function nextSlide() {
  next.classList.contains('disabled') ? false : currentSlide++;
  theChecker();
}

//Previous Slide Function
function prevSlide() {
  prev.classList.contains('disabled') ? false :  currentSlide-- ;
  theChecker();
}

//Remove Class active from image
function removeActive() {
  //Loop on images
  sliderImages.forEach((img)=> img.classList.remove('active'));
}

//Checker Function
function theChecker() {

  //Remove Active from all images
  removeActive();

  //Set Active Class on Current Slide
  sliderImages[currentSlide - 1].classList.add('active');

  //Check if current slide is the first
  currentSlide == 1 ? prev.classList.add('disabled') : prev.classList.remove('disabled');

  //Check if current slide is the last one
  currentSlide == slidesCount ? next.classList.add('disabled') : next.classList.remove('disabled');
}
theChecker();